const Model = require('../models/models');
const mongoose = require('mongoose');

/* CREATE/UPDATE */

exports.save = function (apoio, successCallback, errorCallback) {
    apoio.save(function (err, saved) {
        if (err) errorCallback(err);
        else if (saved == null) errorCallback({
            status: 400,
            message: "Não foi possível guardar dados de apoio"
        });
        else successCallback(saved);
    });
};

/* GET ALL */

exports.getAll = function (successCallback, errorCallback) {
    Model.Apoio.find({}, function (err, apoios) {
        if (err) errorCallback(err);
        else if (apoios == null) errorCallback({
            status: 400,
            message: "Não foi possível encontrar apoios"
        });
        else successCallback(apoios);
    })
};

/* GET ONE */

exports.findById = function (id, successCallback, errorCallback) {
    Model.Apoio.findById(id, function (err, apoio) {
        if (err) errorCallback(err);
        else if (apoio == null) errorCallback({
            status: 404,
            message: "Não foi possível encontrar apoio com id " + id
        });
        else successCallback(apoio);
    })
};

/* DELETE */

exports.delete = function (id, successCallback, errorCallback) {
    Model.Apoio.findByIdAndDelete(id, function (err, deleted) {
        if (err) errorCallback(err);
        else if (deleted == null) errorCallback({
            status: 404,
            message: "Não foi possível apagar apoio"
        });
        else successCallback();
    });
};