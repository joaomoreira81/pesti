const Model = require('../models/models');
const mongoose = require('mongoose');

/* CREATE/UPDATE */

exports.save = function (student, successCallback, errorCallback) {
    student.save(function (err, saved) {
        if (err) errorCallback(err);
        else if (saved == null) errorCallback({
            status: 400,
            message: "Não foi possível guardar dados de aluno"
        });
        else successCallback(saved);

    });
};

/* GET ALL */

exports.getAll = function (successCallback, errorCallback) {
    Model.Student.find({}, function (err, students) {
        if (err) errorCallback(err);
        else if (students == null) errorCallback({
            status: 400,
            message: "Não foi possível encontrar alunos"
        });
        else successCallback(students);
    })
};

/* GET ONE */

exports.findById = function (id, successCallback, errorCallback) {
    Model.Student.findById(id, function (err, student) {
        if (err) errorCallback(err);
        else if (student == null) errorCallback({
            status: 404,
            message: "Não foi possível encontrar aluno com id " + id
        });
        else successCallback(student);
    })
};

exports.findByEmail = function (email, successCallback, errorCallback) {
    Model.Student.findOne({ email: email }, function (err, student) {
        console.log('mail'+student);
        if (err) errorCallback(err);
        else if (student == null) errorCallback({
            status: 404,
            message: "Não foi possível encontrar aluno com email " + email
        });
        else successCallback(student);
    })
    };

    exports.findByEmail = function (email, successCallback, errorCallback) {
    Model.Student.findById(id, function (err, student) {
        if (err) errorCallback(err);
        else if (student == null) errorCallback({
            status: 404,
            message: "Não foi possível encontrar aluno com id " + id
        });
        else successCallback(student);
    })
    };

/* DELETE */

exports.delete = function (id, successCallback, errorCallback) {
    Model.Student.findByIdAndDelete(id, function (err, deleted) {
        if (err) errorCallback(err);
        else if (deleted == null) errorCallback({
            status: 404,
            message: "Não foi possível apagar aluno"
        });
        else successCallback();
    });
};