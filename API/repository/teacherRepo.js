const Model = require('../models/models');
const mongoose = require('mongoose');

/* CREATE/UPDATE */

exports.save = function (teacher, successCallback, errorCallback) {

    teacher.save(function (err, saved) {
       
        if (err) errorCallback(err);
        else if (saved == null) errorCallback({
            status: 400,
            message: "Não foi possível guardar dados de professor"
        });
        else successCallback(saved);
    });
};

/* GET ALL */

exports.getAll = function (successCallback, errorCallback) {
    Model.Teacher.find({}, function (err, teachers) {
        if (err) errorCallback(err);
        else if (teachers == null) errorCallback({
            status: 400,
            message: "Não foi possível encontrar professores"
        });
        else successCallback(teachers);
    })
};

/* GET ONE */

exports.findById = function (id, successCallback, errorCallback) {
    Model.Teacher.findById(id, function (err, teacher) {
        if (err) errorCallback(err);
        else if (teacher == null) errorCallback({
            status: 404,
            message: "Não foi possível encontrar professor com id " + id
        });
        else successCallback(teacher);
    })
};

/* DELETE */

exports.delete = function (id, successCallback, errorCallback) {
    Model.Teacher.findByIdAndDelete(id, function (err, deleted) {
        if (err) errorCallback(err);
        else if (deleted == null) errorCallback({
            status: 404,
            message: "Não foi possível apagar professor"
        });
        else successCallback();
    });
};