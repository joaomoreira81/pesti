const express = require('express');
const router = express.Router();
const apoioServ = require('../service/apoioService');





router.post('/', function(req, res) {
    apoioServ.create(req.body,
        function successCallback(apoio) {
            res.status(201)
            res.headers = {
                'Content-Type': 'application/json'
            }
            res.json(apoio)
        }, function errorCallback(err) {
            res.status(err.status)
            res.headers = {
                'Content-Type': 'application/json'
            }
            res.json(err.message)
        })
})



router.get('/', function(req, res) {
    apoioServ.getAll(function successCallback(apoios) {
        res.status(200)
        console.log(apoios)
        res.json(apoios)

    },  function errorCallback(err) {
        res.status(err.status)
        res.json(err.message)
    })
})

router.get('/:id', function(req, res) {
    apoioServ.findById(req.params.id,
        function successCallback(apoio) {
            res.status(200)
            res.json(apoio)
        },  function errorCallback(err) {
            res.status(err.status)
            res.json(err.message)
        });
})

router.delete('/:id', function(req, res) {
    apoioServ.delete(req.params.id, 
        function successCallback() {
            res.status(204)
            res.json()
        },  function errorCallback(err) {
            res.status(err.status)
            res.json(err.message)
        })
})

module.exports = router;