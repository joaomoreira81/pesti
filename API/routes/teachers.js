const express = require('express');
const router = express.Router();
const teacherServ = require('../service/teacherService');




router.post('/', function(req, res) {
    teacherServ.create(req.body,
        function successCallback(teacher) {
            res.status(201)
            res.headers = {
                'Content-Type': 'application/json'
            }
            res.json(teacher)
        }, function errorCallback(err) {
            res.status(err.status)
            res.headers = {
                'Content-Type': 'application/json'
            }
            res.json(err.message)
        })
})

router.get('/', function(req, res) {
    teacherServ.getAll(function successCallback(teachers) {
        res.status(200)
        res.json(teachers)
    },  function errorCallback(err) {
        res.status(err.status)
        res.json(err.message)
    })
})

router.get('/:id', function(req, res) {
    teacherServ.findById(req.params.id,
        function successCallback(teacher) {
            res.status(200)
            res.json(teacher)
        },  function errorCallback(err) {
            res.status(err.status)
            res.json(err.message)
        });
})

router.delete('/:id', function(req, res) {
    teacherServ.delete(req.params.id, 
        function successCallback() {
            res.status(204)
            res.json()
        },  function errorCallback(err) {
            res.status(err.status)
            res.json(err.message)
        })
})

module.exports = router;