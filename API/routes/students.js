const express = require('express');
const router = express.Router();
const studentServ = require('../service/studentService');
const adapter = require('../service/adapterService');
const request = require('request');




router.post('/', function (req, res) {
        studentServ.create(req.body,
        function successCallback(student) {
            res.status(201)
            res.headers = {
                'Content-Type': 'application/json'
            }
            res.json(student)
        }, function errorCallback(err) {
            res.status(err.status)
            res.headers = {
                'Content-Type': 'application/json'
            }
            res.json(err.message)
        })
})

router.get('/', function (req, res) {
    studentServ.getAll(function successCallback(students) {
        res.status(200)
        res.json(students)
    }, function errorCallback(err) {
        res.status(err.status)
        res.json(err.message)
    })
})

router.get('/id/:id', function (req, res) {
    studentServ.findById(req.params.id,
        function successCallback(student) {
            var redux = adapter.adapter_id(id);
            redux = redux.concat(student);
            res.status(200)
            res.json(redux)
            console.log(redux)
        }, function errorCallback(err) {
            res.status(err.status)
            res.json(err.message)
        });
})



router.delete('/:id', function (req, res) {
    studentServ.delete(req.params.id,
        function successCallback() {
            res.status(204)
            res.json()
        }, function errorCallback(err) {
            res.status(err.status)
            res.json(err.message)
        })
})

module.exports = router;