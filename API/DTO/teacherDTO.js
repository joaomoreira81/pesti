const Model = require('../models/models');
const mongoose = require('mongoose');

exports.fromJSON = function(data) {
    if (data == null || 
        data.name == null || 
        data.email == null) 
        { return null; }
        var teacher = new Model.Teacher();
        teacher._id = new mongoose.Types.ObjectId();
        teacher.name=data.name;
        teacher.email=data.email;

       
    return teacher;
};

exports.toJSON = function(teacher) {
    var parsedTeacher = {
        _id: teacher._id,
        name: teacher.name,
        email: teacher.email,
    };
    
    return parsedTeacher;
};