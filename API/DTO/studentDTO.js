const Model = require('../models/models');
const mongoose = require('mongoose');

exports.fromJSON = function (aluno1, aluno2) {
   
   
    if (aluno1 == null || aluno1.name == null ||
        aluno1.email == null || aluno1.type_impairment == null) {
            console.log('Erro nos dados do aluno!')
        return null;
    }
    if (aluno2 == null || aluno2.id == null ||
        aluno2.name == null || aluno2.class == null ||
        aluno2.year == null || aluno2.email == null ) {
            console.log('Erro nos dados do aluno2!')
        return null;
    }
    
    
    var student = new Model.Student();
    student._id = aluno2._id;
    student.name = aluno2.name;
    student.year = aluno2.year;
    student.class = aluno2.class;
    student.email = aluno2.email;
    student.type_impairment = aluno1.type_impairment;
    student.accepted=aluno1.accepted;
    student.date_registry = aluno1.date_registry,
    student.consent = aluno1.consent

        console.log(typeof student);
        
    return student;
};

exports.toJSON = function (student) {
    var parsedStudent = {
        _id: student._id,
        name: student.name,
        class: student.class,
        year: student.year,
        email: student.email
    };

    return parsedStudent;
};