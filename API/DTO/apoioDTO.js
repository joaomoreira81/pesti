const Model = require('../models/models');
const mongoose = require('mongoose');
const Aluno = require('../repository/StudentRepo');
const Prof = require('../repository/teacherRepo');

exports.fromJSON = function (data) {
    if (data == null ||
        data.name == null ||
        data.email == null ||  
        Aluno.findById(data.student_id) == null ||
        Prof.findById(data.teacher_id) == null) 
        { return null; }
    var apoio = new Model.Apoio();
    apoio._id = mongoose.Types.ObjectId();
    apoio.student_id = data.student_id;
    apoio.teacher_id = data.teacher_id;
    apoio.data = new Date(parseInt(data.data.substr(6)));
    apoio.type_apoio = data.type_apoio;
    return apoio;
};

exports.toJSON = function (apoio) {
    var parsedApoio = {
        _id: apoio._id,
        student: {
            _id: student._id,
            class: student.class,
            year: student.year,
            email: student.email,
            type_impairment:student.type_impairment
        },
        data : apoio.data,
        teacher: {
            _id: teacher._id,
            email: teacher.email,
            name: teacher.name
        },
        type_apoio: apoio.type_apoio
};

return parsedApoio;
};