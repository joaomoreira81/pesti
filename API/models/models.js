const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const teacherSchema = new Schema({
    id: mongoose.Schema.Types.ObjectId,
    email: { type: String, required: true, unique: true },
    name: { type: String, required: true },
    accepted: { type: Boolean , default:false},
    date_registry: { type: Date, default: Date.now, required: true },
    consent: { type: Boolean, required: true }
});


const studentSchema = new Schema({
    _id: { type: String, required: true },
    name: { type: String, required: true },
    email: { type: String, required: true, unique: true },
    year: { type: Number, required: true },
    class: { type: String, required: true },
    type_impairment: {
        type: String,
        required: true,
        enum: ['Dislexia', 'Défice cognitivo', 'PHDA', 'Outra'],
        default: 'Outra'
    },
    accepted: { type: Boolean , default:false},
    date_registry: { type: Date, default: Date.now, required: true },
    consent: { type: Boolean, required: true }
});

const apoioSchema = new Schema({
    id: mongoose.Schema.Types.ObjectId,
    student_id: { type: String, required: true },
    teacher_id: { type: String, required: true },
    data: { type: Date, required: true },
    type_apoio: {
        type: String,
        required: true,
        enum: ['Teste com leitura acompanhada', 'apoio pedagógico',
            'criação de materiais', 'sem alunos'],
        default: 'sem alunos'
    },
});

module.exports.Teacher = mongoose.model('Teacher', teacherSchema);
module.exports.Student = mongoose.model('Student', studentSchema);
module.exports.Apoio = mongoose.model('Apoio', apoioSchema);




