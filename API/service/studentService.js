var studentRep = require('../repository/StudentRepo');
var adapter = require('../service/adapterService');

exports.create = function (studentData, successCallback, errorCallback) {
    adapter.adapteremail(studentData, function success(aluno) {
        console.log('depois adapt0');
        console.log(aluno);
        if (aluno == null) {
            errorCallback({ status: 400, message: "Id do aluno é incompatível" });
            return
        }
        studentRep.save(aluno, function (student) {
            
            successCallback(student);
        }, function (error) {
            console.log(error);
            errorCallback(error);
        });

    }, errorCallback);


};

exports.getAll = function (successCallback, errorCallback) {
    studentRep.getAll(function (students) {
        successCallback(students);
    }, function (error) {
        errorCallback(error);
    });
};

exports.findById = function (id, successCallback, errorCallback) {
    studentRep.findById(id, function (student) {
        successCallback(student)
    }, function (error) {
        errorCallback(error);
    });
};


/* DELETE */

exports.delete = function (id, successCallback, errorCallback) {
    studentRep.delete(id, function () {
        successCallback();
    }, function (error) {
        errorCallback(error);
    });
};






