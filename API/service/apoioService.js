var apoioDTO = require('../DTO/apoioDTO');
var apoioRep = require('../repository/apoioRepo');

exports.create = function (apoioData, successCallback, errorCallback) {
    var apoio = apoioDTO.fromJSON (apoioData,
        function successCallback(student) {
            res.status(201)
            res.headers = {
                'Content-Type': 'application/json'
            }
            res.json(student)
        }, function errorCallback(err) {
            res.status(err.status)
            res.headers = {
                'Content-Type': 'application/json'
            }
            res.json(err.message)
        });
    if (apoio == null) {
        errorCallback({ status: 400, message: "Dados de apoio são inválidos" });
        return
    }
    apoioRep.save(apoio,function(apoio){
        successCallback(apoio);
    }, function(error) {
        errorCallback(error);
    });
};

exports.getAll = function (successCallback, errorCallback) {
    apoioRep.getAll(function(apoios) {
        successCallback(apoios);
    }, function(error) {
        errorCallback(error);
    });
};

exports.findById = function(id, successCallback, errorCallback) {
    apoioRep.findById(id, function(apoio) {
        successCallback(apoio)
    }, function(error) {
        errorCallback(error);
    });
};


/* DELETE */

exports.delete = function (id, successCallback, errorCallback) {
    apoioRep.delete(id, function() {
        successCallback();
    }, function(error) {
        errorCallback(error);
    });
};



