var teacherDTO = require('../DTO/teacherDTO');
var teacherRep = require('../repository/teacherRepo');

exports.create = function (teacherData, successCallback, errorCallback) {
    var teacher = teacherDTO.fromJSON(teacherData);
    
    if (teacher == null) {
        errorCallback({ status: 400, message: "Dados do professor são inválidos" });
        return
    }
    teacherRep.save(teacher,function(teacher){
        successCallback(teacher);
    }, function(error) {
        errorCallback(error);
    });
};

exports.getAll = function (successCallback, errorCallback) {
    teacherRep.getAll(function(teachers) {
        successCallback(teachers);
    }, function(error) {
        errorCallback(error);
    });
};

exports.findById = function(id, successCallback, errorCallback) {
    teacherRep.findById(id, function(teacher) {
        successCallback(teacher)
    }, function(error) {
        errorCallback(error);
    });
};


/* DELETE */

exports.delete = function (id, successCallback, errorCallback) {
    teacherRep.delete(id, function() {
        successCallback();
    }, function(error) {
        errorCallback(error);
    });
};




    

    